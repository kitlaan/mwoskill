
function MwoSkills(hexagonGrid, lineGrid) {
    this.version = "0.4";

    this.maxValidSkillCount = 91;

    this.hex = hexagonGrid;
    this.lines = lineGrid;

    var offset = 0;
    this.hex.setOrigin((this.hex.canvas.width - this.hex.width) / 2 + offset, 0);
    this.lines.setOrigin((this.lines.canvas.width - this.lines.width) / 2 + offset, 0);

    //this.debug = true;

    this.hookupCall("importmwo", "click", this.importMwoCode);
    this.hookupCall("exportmwo", "click", this.exportMwoCode);
    this.hookupCall("downloader", "click", this.downloadEvent);
    this.hookupCall("permlink", "click", this.genPermLinkEvent);
    this.hookupCall("selectallnodes", "click", this.selectallnodesEvent);
    this.hookupCall("clearnodes", "click", this.clearnodesEvent);
    this.hookupCall("cleareverything", "click", this.clearEverythingEvent);
    this.hookupCall("toggledisabled", "click", this.toggleDisabledNodesEvent);
    this.hookupCall("factionSelect", "change", this.changeMechListEvent);
    this.hookupCall("weightSelect", "change", this.changeMechListEvent);

    this.hookupCall(window, "keypress", this.keyEvent);
    this.hookupCall(window, "keydown", this.keyUpDownEvent);
    this.hookupCall(window, "keyup", this.keyUpDownEvent);

    this.isMobile = (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf("IEMobile") !== -1);

    this.resetSkills();

    this.disabledNodeVisible = true;
    this.activeSection = "";
    this.unlockedSkills = [];
    this.sectionCount = {};
    this.totalSkillCount = 0;

    this.selectedFaction = "IS";
    this.selectedWeight = "Heavy";
};

MwoSkills.prototype.resetSkills = function() {
    this.lines.clear();
    this.hex.clear();

    // list of versions, 'skill-versions.json'
    this.versionFiles = [];

    // list of skill information, 'skill-versions.json'
    this.skillTypes = {};

    // data loaded from the skill itself
    this.skillDate = "";
    this.skillVersion = "";
    this.skillSections = [];
    this.skillData = {};
};

MwoSkills.prototype.initialize = function(preset, section, mwohash) {
    this.resetSkills();

    this.loadSessionState(preset, section, mwohash);

    this.loadLocalJSON("skill-types.json", function(data) {
        this.skillTypes = data;
    });

    this.loadLocalJSON("skill-versions.json", function(data) {
        this.versionFiles = data;
        this.populateSkillModal();
    });
};

MwoSkills.prototype.loadSkills = function(filename) {
    this.loadLocalJSON(filename, function(data) {
        if (data && "sections" in data) {
            this.skillDate = data.date;
            this.skillVersion = data.version;

            // put the data somewhere convenient
            this.skillSections = [];
            this.skillData = {};
            for (var s of data.sections) {
                this.skillSections.push(s.title);
                this.skillData[s.title] = s;
            }

            // version information
            var geninfo = document.getElementById('GeneralInfo');
            this.clearElement(geninfo);
            geninfo.appendChild(document.createElement('br'));
            geninfo.appendChild(document.createTextNode(this.skillVersion));
            geninfo.appendChild(document.createElement('br'));
            geninfo.appendChild(document.createTextNode("(" + this.skillDate + ")"));

            // left menu for "sections"
            var nav = document.getElementById('NavPane');
            this.clearElement(nav);
            var lastCategory = "";
            for (var s of data.sections) {
                if (lastCategory != s.category) {
                    lastCategory = s.category;

                    var c = document.createElement('span');
                    c.appendChild(document.createTextNode(s.category));
                    nav.appendChild(c);
                }

                var me = this;
                var a = document.createElement('a');
                a.appendChild(document.createTextNode(s.title));
                a.target = s.title;
                a.id = "nav_" + s.title;
                a.onclick = function() {
                    me.setActiveSection(this.target);
                };
                nav.appendChild(a);
            }

            // check for deferred work.
            if (this.pendingSkillLoad) {
                this.pendingSkillLoad();
                this.pendingSkillLoad = null;
            }

            // clean up unlocked skills
            var oldunlocked = this.unlockedSkills;
            this.unlockedSkills = [];
            this.totalSkillCount = 0;
            for (var s in this.skillData) {
                for (var c of this.skillData[s].cells) {
                    this.totalSkillCount++;

                    var key = s + "," + c.col + "," + c.row;
                    try {
                        if (oldunlocked.indexOf(key) >= 0) {
                            this.unlockedSkills.push(key);
                        }
                    }
                    catch(e) { }
                }
            }

            this.updateSkillCount();
            this.computeQuirkRollup();

            // make sure the section that was active, can still be active...
            this.setActiveSection(this.activeSection);
        }
    });
};

MwoSkills.prototype.populateSkillModal = function() {
    var options = document.getElementById("SkillVersions").options;
    for (var v of this.versionFiles) {
        var o = document.createElement("option");
        o.text = v.version + " (" + v.date + ")";
        o.value = v.file;
        options.add(o);
    }

    var me = this;
    var button = document.getElementById("SkillVersionsButton");
    button.onclick = function() {
        me.loadSkills(options[options.selectedIndex].value);

        var modal = document.getElementById("SelectVersionModal");
        modal.style.display = "none";
        window.onclick = undefined;

        me.changedState();
    }
};

MwoSkills.prototype.renderSection = function(section) {
    if (!(section in this.skillData)) {
        return false;
    }

    this.lines.clear();
    this.hex.clear();

    this.renderSectionOnHexGrid(section, this.hex, this.lines);
};

MwoSkills.prototype.renderSectionOnHexGrid = function(section, hexGrid, lineGrid) {
    var ctx = lineGrid.context;

    var origin = lineGrid.getOrigin();
    origin.x += lineGrid.width / 2;

    // NOTE: HERE BE MAGIC NUMBERS!
    ctx.font = "30px sans-serif";
    ctx.fillStyle = "#aaa";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(this.skillData[section].title, origin.x, origin.y + 24);

    var txtWidth = ctx.measureText(this.skillData[section].title).width;

    var countCells = (section in this.sectionCount) ? this.sectionCount[section] : 0;
    ctx.font = "20px sans-serif";
    ctx.fillStyle = "#888";
    ctx.fillText("(" + countCells + ")",
                 origin.x + (txtWidth / 2) + 30, origin.y + 27);

    function sortCells(a, b) {
        return (a.row * 2 + (a.col % 2 == 0 ? 1 : 0)) -
               (b.row * 2 + (b.col % 2 == 0 ? 1 : 0));
    }

    var invalidCells = this.computeSectionValid(section);

    // draw the cells from top-to-bottom
    var cells = this.skillData[section].cells;
    for (var c of cells.sort(sortCells)) {
        var valid = invalidCells.indexOf(c.col + "," + c.row) < 0;
        this.drawCellOnHexGrid(section, c, hexGrid, lineGrid, valid);
    }

    // draw informational stuffs
    var errorString = null;
    if (lineGrid == this.lines && Object.keys(this.unlockedSkills).length > this.maxValidSkillCount) {
        errorString = "TOO MANY SKILLS";
    }
    else if (invalidCells.length > 0) {
        errorString = "INVALID TREE";
    }

    if (errorString) {
        var w = 30;
        var h = w * Math.sqrt(3)/2;
        var offX = (lineGrid.width * 5) / 2 - 20;
        var offY = (lineGrid.height / 2) + 36 - h/2;

        lineGrid.drawTriangle(
                [{x:offX+0,    y:offY+0},
                 {x:offX+w/2, y:offY+h},
                 {x:offX-w/2, y:offY+h}],
                {stroke:'#f33', fill:'#f33'});

        ctx.fillStyle = "#ff0";
        ctx.fillText("!",
                     origin.x + offX - lineGrid.width/2,
                     origin.y + offY + h/2 + 2);

        ctx.font = "bold 16px sans-serif";
        ctx.fillStyle = "#f33";
        ctx.textAlign = "left";
        ctx.fillText(errorString,
                     origin.x + offX - w/2 - 10,
                     origin.y + offY + h/2);
    }
};

MwoSkills.prototype.drawCellOnHexGrid = function(section, cell, hexGrid, lineGrid, valid) {
    var colors = this.getSkillColors(section, cell.col, cell.row, valid);

    linecolors = JSON.parse(JSON.stringify(colors));
    if (!valid) {
        linecolors.dash = [3, 2];
    }
    for (var l of cell.links) {
        lineGrid.drawLine(cell.col, cell.row, l.col, l.row, linecolors);
    }

    hexGrid.drawHexAtColRow(cell.col, cell.row, colors);
    if (this.debug) {
        hexGrid.drawHexDebugAtColRow(cell.col, cell.row);
    }

    var origin = hexGrid.getOrigin();

    var center = hexGrid.getCenterAtColRow(cell.col, cell.row);
    center.x += origin.x;
    center.y += origin.y;

    var ctx = hexGrid.context;

    // draw the title

    // NOTE: HERE BE MAGIC NUMBERS!
    ctx.font = "bold 13px sans-serif";
    ctx.fillStyle = colors.text;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";

    var words = (cell.type).split(" ");

    var title1 = "";
    if (words.length > 1) {
        title1 = words.slice(0, words.length - 1).join(" ");
    }

    var title2 = words[words.length - 1];
    if (cell.level) {
        // NOTE: the "level" just clutters; remove it
        if (this.debug) {
            title2 += " " + cell.level;
        }
    }

    // NOTE: HERE BE MAGIC NUMBERS!
    if (title1) {
        var spacing = 14;
        var off = 5;
        ctx.fillText(title1, center.x, center.y + off - spacing);
        ctx.fillText(title2, center.x, center.y + off);
    }
    else {
        ctx.fillText(title2, center.x, center.y - 6);
    }

    // draw value

    // NOTE: HERE BE MAGIC NUMBERS!
    ctx.font = "15px sans-serif";
    ctx.fillStyle = colors.text;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";

    var value = this.formatSkillValue(cell.type, this.getSkillValue(cell.value));
    if (value) {
        // NOTE: HERE BE MAGIC NUMBERS!
        ctx.fillText(value, center.x, center.y + 24);
    }
};

MwoSkills.prototype.clickEventHexGrid = function(e) {
    var origin = this.hex.getOrigin();

    var localX = e.pageX - origin.x;
    var localY = e.pageY - origin.y;

    // right-click is for download section, so clear fancy things
    if (e.button == 2) {
        this.lastOverTile = undefined;
        this.renderHoverType = undefined;
        this.renderSection(this.activeSection);
    }

    var tile = this.hex.getSelectedTile(localX, localY);
    if (tile && e.button == 0) {
        var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
        if (skill) {
            if (!e.shiftKey) {
                this.toggleSkillState(this.activeSection, tile.col, tile.row);
                this.renderSection(this.activeSection);
            }
            else {
                var active = !this.isUnlocked(this.activeSection, tile.col, tile.row);
                var cells = this.skillData[this.activeSection].cells;
                for (var c of cells) {
                    if (c.type == skill.type) {
                        this.setSkillState(this.activeSection, c.col, c.row, active);
                    }
                }
                this.renderSection(this.activeSection);
            }

            this.changedState();
        }
    }
};

MwoSkills.prototype.mousemoveEventHexGrid = function(e) {
    if (this.isMobile) {
        return;
    }

    var origin = this.hex.getOrigin();

    var localX = e.pageX - origin.x;
    var localY = e.pageY - origin.y;

    var cursor = "default";

    var tile = this.hex.getSelectedTile(localX, localY);

    if (!tile || !this.lastOverTile ||
            tile.col != this.lastOverTile.col || tile.row != this.lastOverTile.row) {
        window.clearTimeout(this.showTip);
        this.showTip = undefined;
        var tip = document.querySelector('.tooltip');
        if (tip) {
            tip.remove();
        }
    }

    if (tile) {
        var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
        if (skill) {
            cursor = "pointer";

            var descr = "";
            if (skill.type in this.skillTypes) {
                descr = this.skillTypes[skill.type].descr;
            }

            if (!this.showTip && !this.debug) {
                var me = this;
                this.showTip = window.setTimeout(function() {
                    me.createToolTip(tile.col, tile.row, descr);
                }, 700);
            }
        }
        else if (this.editor) {
            cursor = "crosshair";

            // no-op for now. ideally, we'd highlight the "dummy cell"
        }

        if (cursor != "default") {
            var changedtile = !this.lastOverTile ||
                              (this.lastOverTile.col != tile.col || this.lastOverTile.row != tile.row);
            this.lastOverTile = tile;

            this.renderHoverType = skill ? skill.type : undefined;

            if (changedtile) {
                if (e.buttons & 1) {
                    this.clickEventHexGrid(e);
                }
                else {
                    this.renderSection(this.activeSection);
                }
            }
        }
    }
    else if (this.lastOverTile) {
        this.lastOverTile = undefined;
        this.renderHoverType = undefined;
        this.renderSection(this.activeSection);
    }

    this.hex.canvas.style.cursor = cursor;
};

MwoSkills.prototype.createToolTip = function(col, row, text) {
    var canvasProps = this.hex.canvas.getBoundingClientRect();
    var origin = lineGrid.getOrigin();
    var center = this.hex.getCenterAtColRow(col, row);

    center.x += window.scrollX + canvasProps.left + origin.x - (this.hex.width - this.hex.side);
    center.y += window.scrollY + canvasProps.top  + origin.y + this.hex.height / 2;

    var oldtip = document.querySelector('.tooltip');
    if (oldtip) {
        oldtip.remove();
    }

    var tip = document.createElement('div');
    tip.className = 'tooltip';
    tip.appendChild(document.createTextNode(text));

    tip.style.top = (center.y + 6) + "px";
    tip.style.left = (center.x - 6) + "px";

    var first = document.body.firstChild;
    first.parentNode.insertBefore(tip, first);
};

MwoSkills.prototype.loadLocalJSON = function(filename, callback) {
    var bound = callback.bind(this);

    var xr = new XMLHttpRequest();
    xr.overrideMimeType("application/json");
    xr.open("GET", filename, true);
    xr.timeout = 5000;
    xr.ontimeout = xr.onerror = function() {
        bound(null);
    };
    xr.onload = function() {
        if (xr.status == 200) {
            bound(JSON.parse(xr.responseText));
        }
        else {
            xr.onerror();
        }
    };
    xr.send();
};

MwoSkills.prototype.loadJSON = function(key, callback) {
    var bound = callback.bind(this);

    this.loadArchive(key, callback, function() {
        if (key.indexOf("jsonbin1.") == 0) {
            this.loadJSONBIN1(key.substring(9), callback);
        }
        else {
            this.loadJSONBLOB(key, callback);
        }
    });
};

// TODO: refactor so there's just one XHR helper call again?
MwoSkills.prototype.loadArchive = function(key, goodcallback, badcallback) {
    var goodbound = goodcallback.bind(this);
    var badbound = badcallback.bind(this);

    var filename = "../mwoskill_json/json/" + key;

    var xr = new XMLHttpRequest();
    xr.overrideMimeType("application/json");
    xr.open("GET", filename, true);
    xr.timeout = 500;
    xr.ontimeout = xr.onerror = function() {
        console.log("going to the network!");
        badbound(key, goodcallback);
    };
    xr.onload = function() {
        if (xr.status == 200) {
            console.log("got from archive");
            goodbound(JSON.parse(xr.responseText), "archive");
        }
        else {
            xr.onerror();
        }
    };
    xr.send();
};

MwoSkills.prototype.loadJSONBLOB = function(key, callback) {
    var filename = "https://jsonblob.com/api/jsonBlob/" + key;

    var bound = callback.bind(this);

    var xr = new XMLHttpRequest();
    xr.overrideMimeType("application/json");
    xr.open("GET", filename, true);
    xr.timeout = 5000;
    xr.ontimeout = xr.onerror = function() {
        bound(null, "jsonblob.com");
    };
    xr.onload = function() {
        if (xr.status == 200) {
            bound(JSON.parse(xr.responseText), "jsonblob.com");
        }
        else {
            xr.onerror();
        }
    };
    xr.send();
};

MwoSkills.prototype.loadJSONBIN1 = function(key, callback) {
    var filename = "https://api.jsonbin.io/b/" + key;

    var bound = callback.bind(this);

    var xr = new XMLHttpRequest();
    xr.overrideMimeType("application/json");
    xr.open("GET", filename, true);
    xr.timeout = 5000;
    xr.ontimeout = xr.onerror = function() {
        bound(null, "jsonbin.io");
    };
    xr.onload = function() {
        if (xr.status == 200) {
            var json = JSON.parse(xr.responseText);
            if (json && json.snippet) {
                // sigh, my bad
                json = JSON.parse(json.snippet);
            }
            bound(json, "jsonbin.io");
        }
        else {
            xr.onerror();
        }
    };
    xr.send();
};

MwoSkills.prototype.getSkillColors = function(section, col, row, valid) {
    var unlocked = this.isUnlocked(section, col, row);
    var skill = this.getSkillFromCell(section, col, row);

    function shadeColor(color, percent) {
        var f = parseInt(color.slice(1), 16);
        var t = percent < 0 ? 0 : 255;
        var p = percent < 0 ? percent * -1 : percent;
        var R = f >> 16;
        var G = f >> 8 & 0x00FF;
        var B = f & 0x0000FF;
        return "#" + (0x1000000 +
                      (Math.round((t-R)*p)+R)*0x10000 +
                      (Math.round((t-G)*p)+G)*0x100 +
                      (Math.round((t-B)*p)+B)).toString(16).slice(1);
    }

    // NOTE: HERE BE MAGIC NUMBERS!

    var colors = {};

    var colormap = this.getStyleForCssId('#ColorMap_' + skill.type.split(/\W/).join(''), 'color');
    if (colormap) {
        function hex(x) {
            var hexDigits = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
            return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
        }
        function rgb2hex(rgb) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }
        colors.fill = rgb2hex(colormap);
    }
    else {
        colors.fill = '#484848';
    }

    if (unlocked) {
        colors.stroke = "#999999";
        colors.text = "#e0e0e0";
        colors.width = 6;
    }
    else {
        colors.stroke = "#373737";
        colors.text = "#808080";
        colors.fill = shadeColor(colors.fill, -0.5);
    }

    if (!valid) {
        colors.stroke = "#ff3333";
        colors.dash = [7, 3];
    }

    if ((!unlocked || !valid) && !this.disabledNodeVisible) {
        var drop = -0.64;
        colors.stroke = shadeColor(colors.stroke, drop);
        colors.fill = shadeColor(colors.fill, drop);
        colors.text = shadeColor(colors.text, drop);
    }
    else if (this.renderHoverType == skill.type) {
        colors.glow = [12, "#dd0"];
    }

    return colors;
};

MwoSkills.prototype.isUnlocked = function(section, col, row) {
    if (!this.unlockedSkills) {
        return false;
    }

    var key = section + "," + col + "," + row;
    return (this.unlockedSkills.indexOf(key) >= 0);
};

MwoSkills.prototype.toggleSkillState = function(section, col, row) {
    var key = section + "," + col + "," + row;

    var index = this.unlockedSkills.indexOf(key);
    if (index >= 0) {
        this.unlockedSkills.splice(index, 1);
    }
    else {
        this.unlockedSkills.push(key);
    }

    this.changedState();
};

MwoSkills.prototype.setSkillState = function(section, col, row, active) {
    var key = section + "," + col + "," + row;

    var index = this.unlockedSkills.indexOf(key);
    if (index >= 0 && !active) {
        this.unlockedSkills.splice(index, 1);
    }
    else if (index < 0 && active) {
        this.unlockedSkills.push(key);
    }

    this.changedState();
}

MwoSkills.prototype.getSkillFromCell = function(section, col, row) {
    if (!(section in this.skillData)) {
        return;
    }

    var cells = this.skillData[section].cells;
    for (var c of cells) {
        if (col == c.col && row == c.row) {
            return c;
        }
    }
};

MwoSkills.prototype.forEachUnlockedSkill = function(callback) {
    var bound = callback.bind(this);

    for (var key of this.unlockedSkills) {
        var fields = key.split(',');
        bound(fields[0], parseInt(fields[1]), parseInt(fields[2]));
    }
};

MwoSkills.prototype.setActiveSection = function(section) {
    if (!section || !this.skillSections.includes(section)) {
        section = this.skillSections[0];
    }

    var a;

    if (this.activeSection) {
        a = document.getElementById("nav_" + this.activeSection);
        if (a) {
            a.style.backgroundColor = "";
        }
    }

    // NOTE: HERE BE MAGIC NUMBERS!
    a = document.getElementById("nav_" + section);
    if (a) {
        a.style.backgroundColor = "#977";

        this.activeSection = section;

        this.saveSessionState();
        this.renderSection(this.activeSection);
    }
};

MwoSkills.prototype.clearElement = function(elem) {
    while (elem.hasChildNodes()) {
        elem.removeChild(elem.lastChild);
    }
};

MwoSkills.prototype.mirrorEvent = function() {
    var canvas = document.createElement('canvas');

    canvas.width = this.hex.canvas.width;
    canvas.height = this.hex.canvas.height;

    var context = canvas.getContext('2d');

    var bodyStyle = getComputedStyle(document.getElementsByTagName("body")[0]);
    context.fillStype = bodyStyle.backgroundColor;

    context.fillRect(0, 0, canvas.width, canvas.height);

    context.drawImage(this.lines.canvas, 0, 0);
    context.drawImage(this.hex.canvas, 0, 0);

    this.drawWatermark(canvas);

    this.hex.context.drawImage(canvas, 0, 0);
};

MwoSkills.prototype.renderAllSections = function(title) {
    var numSections = this.skillSections.length;
    if (numSections == 0) {
        return;
    }

    var wantedImageWidth = 2800;

    var maxRowWidth = 0;
    var maxRowHeight = 0;

    var sectionPlacement = {};
    var rowMaxWidths = [];
    var renderRow = 0;
    var renderRowWidth = 0;
    for (var s of this.skillSections) {
        var dim = this.computeSectionDimensions(s);

        if (renderRowWidth == 0 && dim.width > wantedImageWidth) {
            wantedImageWidth = dim.width;
        }
        if (dim.height > maxRowHeight) {
            maxRowHeight = dim.height;
        }
        if (maxRowWidth < renderRowWidth) {
            maxRowWidth = renderRowWidth;
        }

        if (dim.width + renderRowWidth > wantedImageWidth) {
            renderRowWidth = 0;
            renderRow++;
        }

        sectionPlacement[s] = { x: renderRowWidth,
                                width: dim.width,
                                row: renderRow };

        renderRowWidth += dim.width;

        rowMaxWidths[renderRow] = renderRowWidth;
    }

    var canvas = document.createElement('canvas');

    canvas.width = maxRowWidth + 400;
    canvas.height = (renderRow + 1) * maxRowHeight;

    var context = canvas.getContext('2d');

    var bodyStyle = getComputedStyle(document.getElementsByTagName("body")[0]);

    context.fillStyle = bodyStyle.backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height);

    this.drawWatermark(canvas);

    var grid = new HexagonGrid(canvas, this.hex.radius, this.hex.spacing);

    for (var s of this.skillSections) {
        var dim = sectionPlacement[s];

        var off = (maxRowWidth - rowMaxWidths[dim.row]) / 2;

        var y = (dim.row * maxRowHeight);

        grid.setOrigin(dim.x + dim.width/2 + off, y);
        this.renderSectionOnHexGrid(s, grid, grid);
    }

    context.textAlign = "left";
    context.textBaseline = "bottom";

    if (title) {
        context.font = "bold 24px sans-serif";
        context.fillStyle = "#ccc";
        context.fillText(title, maxRowWidth + 30, 180);
    }

    context.font = "18px sans-serif";
    context.fillStyle = "#aaa";
    context.fillText("(" + this.selectedFaction + " " + this.selectedWeight + " values)",
                     maxRowWidth + 30, 210);

    if (this.lastPermId) {
        context.font = "14px monospace";
        context.fillStyle = "#aaa";
        context.textAlign = "left";
        context.fillText(location.href.split('?')[0] + "?h=" + this.lastPermId,
                         30, canvas.height - 20);
    }

    var unlockedCount = Object.keys(this.unlockedSkills).length;
    var counter = document.getElementById("NodesUnlocked");
    context.font = "18px sans-serif";
    context.fillStyle = unlockedCount > this.maxValidSkillCount ? "#f00" : "#888";
    context.textAlign = "left";
    context.fillText(counter.textContent + " unlocked nodes", maxRowWidth + 30, 250);

    var count = 0;
    var height = 300;
    for (var n of document.querySelectorAll("#QuirkRollup tr")) {
        var rowstyle = getComputedStyle(n);

        var left = n.childNodes[0];
        var leftstyle = getComputedStyle(left);

        var right = n.childNodes[1];
        var rightstyle = getComputedStyle(right);

        context.font = rowstyle.fontSize + " sans-serif";
        context.fillStyle = (count % 2 == 0) ? "#888" : "#bbb";
        context.textAlign = "left";
        context.textBaseline = "bottom";

        context.fillText(left.textContent, maxRowWidth + 30, height);

        context.textAlign = "right";
        context.fillText(right.textContent, maxRowWidth + parseInt(rowstyle.width), height);

        height += parseInt(rowstyle.height);
        count++;
    }

    return canvas;
};

MwoSkills.prototype.keyUpDownEvent = function(evt) {
    switch (evt.key) {
        case 'Escape':
            if (evt.type == "keyup" && this.activeModalCb) {
                this.activeModalCb();
            }
            break;
        default:
            break;
    }
};

MwoSkills.prototype.keyEvent = function(evt) {
    switch (evt.key) {
        case '=':
            if (this.editor) {
                this.disableEditor();
            }
            else {
                this.enableEditor();
            }
            break;
        default:
            break;
    }

    if (!this.editor) {
        return;
    }

    switch (evt.key) {
        case 'a':
            this.editorAddTile(this.lastOverTile);
            break;
        case 's':
            this.editorEditTile(this.lastOverTile);
            break;
        case 'd':
            this.editorDeleteTile(this.lastOverTile);
            break;
        case 'z':
            this.editorAddLink(this.lastOverTile, -1);
            break;
        case 'x':
            this.editorAddLink(this.lastOverTile, 0);
            break;
        case 'c':
            this.editorAddLink(this.lastOverTile, 1);
            break;
        case 'v':
            break;
        default:
            break;
    }
};

MwoSkills.prototype.genPermLinkEvent = function() {
    if (this.permlinkDisabled) {
        return;
    }

    var unlocked = [];
    this.forEachUnlockedSkill(function(section, col, row) {
        var skill = this.getSkillFromCell(section, col, row);
        unlocked.push([section, skill.type, parseInt(skill.level)]);
    });

    var imp = new MwoSkillImporter();
    var code = imp.serialize(unlocked);

    this.lastPermId = code;

    var content = document.getElementById("generalContent");

    var url = location.href.split('?')[0].split('#')[0];
    url += "?h=" + code;
    url += "#s=" + encodeURIComponent(this.activeSection);

    var ahref = document.createElement('a');
    ahref.href = url;
    ahref.appendChild(document.createTextNode(url));

    this.generateContent(content, [
            "Permalink:",
            document.createElement('br'),
            ahref]);
    this.makeModal("GeneralModal", null, null);

    window.history.pushState(null, "modified", url);
};

MwoSkills.prototype.importMwoCode = function() {
    var code = window.prompt("MWO Skill Code");
    if (!code) {
        return;
    }
    code.trim();
    if (code == "") {
        return;
    }

    var imp = new MwoSkillImporter();
    var nodes = imp.deserialize(code);

    // clear everything
    this.unlockedSkills = [];

    for (var n of nodes) {
        var name = n[1].toLowerCase();
        for (var c of this.skillData[n[0]].cells) {
            if (c.type.toLowerCase() == name && c.level == n[2]) {
                var key = n[0] + "," + c.col + "," + c.row;
                this.unlockedSkills.push(key);
            }
        }
    }

    this.pendingSkillLoad = null;

    this.changedState();
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.exportMwoCode = function() {
    // TODO: check error state; button should reflect

    var unlocked = [];
    this.forEachUnlockedSkill(function(section, col, row) {
        var skill = this.getSkillFromCell(section, col, row);
        unlocked.push([section, skill.type, parseInt(skill.level)]);
    });

    var imp = new MwoSkillImporter();
    var code = imp.serialize(unlocked);

    alert(code);
    // TODO: update location
    // TODO: copy to clipboard
    // TODO: modal?
};

MwoSkills.prototype.downloadEvent = function() {
    var title = window.prompt("Save Using Title?");
    title.trim();

    var canvas = this.renderAllSections(title);

    var button = document.getElementById("downloader");
    if (title) {
        button.download = "mwoskill (" + title + ").png";
    } else {
        button.download = "mwoskill.png";
    }
    button.href = canvas.toDataURL("image/png");
};

MwoSkills.prototype.selectallnodesEvent = function() {
    var cells = this.skillData[this.activeSection].cells;
    for (var c of cells) {
        var key = this.activeSection + "," + c.col + "," + c.row;

        var index = this.unlockedSkills.indexOf(key);
        if (index < 0) {
            this.unlockedSkills.push(key);
        }
    }

    this.changedState();
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.clearnodesEvent = function() {
    for (var i = this.unlockedSkills.length - 1; i >= 0; i--) {
        var fields = this.unlockedSkills[i].split(',');

        if (this.activeSection == fields[0]) {
            this.unlockedSkills.splice(i, 1);
        }
    }

    this.changedState();
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.clearEverythingEvent = function() {
    this.unlockedSkills = [];

    this.changedState();
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.toggleDisabledNodesEvent = function() {
    this.disabledNodeVisible = !this.disabledNodeVisible;
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.changeMechListEvent = function() {
    var factionSelect = document.getElementById("factionSelect");
    var weightSelect = document.getElementById("weightSelect");

    var changed = factionSelect.value != this.selectedFaction ||
                  weightSelect.value != this.selectedWeight;

    this.selectedFaction = factionSelect.value;
    this.selectedWeight = weightSelect.value;

    this.saveSessionState();
    this.computeQuirkRollup();
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.drawWatermark = function(canvas) {
    var context = canvas.getContext('2d');

    // NOTE: HERE BE MAGIC NUMBERS!
    context.font = "10px sans-serif";
    context.fillStyle = "#888";
    context.textAlign = "right";
    context.textBaseline = "bottom";

    context.fillText("generated with kitlaan.gitlab.io/mwoskill v" + this.version,
                     canvas.width - 10, canvas.height - 4);
    context.fillText("MWO version " + this.skillVersion + " (" + this.skillDate + ")",
                     canvas.width - 10, canvas.height - 18);
};

MwoSkills.prototype.updateSkillCount = function() {
    this.sectionCount = {};

    var count = 0;
    this.forEachUnlockedSkill(function(section, col, row) {
        count++;

        if (!(section in this.sectionCount)) {
            this.sectionCount[section] = 0;
        }
        this.sectionCount[section]++;
    });

    var counter = document.getElementById("NodesUnlocked");
    this.clearElement(counter)
    counter.appendChild(document.createTextNode(count));

    for (var section of this.skillSections) {
        var txt = section + " (" +
                  ((section in this.sectionCount) ? this.sectionCount[section] : 0) + ")";
        var a = document.getElementById("nav_" + section);
        this.clearElement(a);
        a.appendChild(document.createTextNode(txt));
    }

    // disable permlink button?
    this.permlinkDisabled = (count == 0 || count == this.totalSkillCount);
    var permlink = document.getElementById("permlink");
    permlink.style.color = this.permlinkDisabled ? '#808080' : null;
    permlink.style.backgroundColor = this.permlinkDisabled ? '#ccaaaa' : null;
};

MwoSkills.prototype.getSkillValue = function(value) {
    if (typeof value == 'number') {
        return value;
    }

    if (this.selectedFaction in value) {
        value = value[this.selectedFaction];
    }
    else if ("_" in value) {
        value = value["_"];
    }
    else {
        return "?";
    }

    if (typeof value == 'number') {
        return value;
    }

    if (this.selectedWeight in value) {
        return value[this.selectedWeight];
    }
    else if ("_" in value) {
        return value["_"];
    }
    else {
        return "?";
    }
};

MwoSkills.prototype.formatSkillValue = function(name, value) {
    var output = "";

    if (typeof value == 'number') {
        // deal with javascript floating point accuracy
        var n = value < 0 ? -value : value;
        n = parseFloat(n).toPrecision(5).replace(/0+$/, '').replace(/\.$/, '');

        output += (value > 0) ? "+" : (value < 0) ? "\u2212" : "";
        output += n;
    }
    else {
        console.log("why? " + value);
        output += value;
    }

    if (output) {
        if (name in this.skillTypes) {
            var skilltype = this.skillTypes[name].type;
            switch (skilltype) {
                case "percent":
                    output += "%";
                    break;
                case "number":
                    break;
                case "none":
                    output = "";
                    break;
                default:
                    console.error("unknown value type '" + skilltype + "'");
                    break;
            }
        }
        else if (name === "") {
            output = "";
        }
        else {
            console.error("missing skill type '" + name + "'");
        }
    }

    return output;
};

MwoSkills.prototype.computeSectionValid = function(section) {
    // populate with all the cells in this section
    var rowcol = {};
    for (var c of this.skillData[section].cells) {
        if (!(c.row in rowcol)) {
            rowcol[c.row] = {};
        }
        rowcol[c.row][c.col] = c.links;
    }

    // starting with the root, remove unlocked cells that are direct
    // decendents of the root
    var unlockedCells = [];
    if (this.isUnlocked(section, 0, 0)) {
        unlockedCells.push({r:0, c:0});
    }

    while (unlockedCells.length > 0) {
        var coord = unlockedCells.pop();
        if (coord.r in rowcol && coord.c in rowcol[coord.r]) {
            for (var l of rowcol[coord.r][coord.c]) {
                if (this.isUnlocked(section, l.col, l.row)) {
                    unlockedCells.push({r:l.row, c:l.col});
                }
            }
            delete rowcol[coord.r][coord.c];
        }
    }

    // make a list of invalid cells
    var invalidcells = [];
    for (var r in rowcol) {
        for (var c in rowcol[r]) {
            if (this.isUnlocked(section, c, r)) {
                invalidcells.push(c + "," + r);
            }
        }
    }
    return invalidcells;
};

MwoSkills.prototype.computeSectionDimensions = function(section) {
    var maxY = 0;
    var minX = 0;
    var maxX = 0;

    for (var c of this.skillData[section].cells) {
        var offset = this.hex.getOffsetAtColRow(c.col, c.row);
        if (offset.x < minX) {
            minX = offset.x;
        }
        if (offset.x + this.hex.width + this.hex.spaceY > maxX) {
            maxX = offset.x + this.hex.width + this.hex.spaceY;
        }
        if (offset.y + this.hex.height + this.hex.spaceY > maxY) {
            maxY = offset.y + this.hex.height + this.hex.spaceY;
        }
    }

    if (Math.abs(minX) > maxX) {
        maxX = Math.abs(minX);
    }

    return { width: maxX * 2, height: maxY };
};

MwoSkills.prototype.computeQuirkRollup = function() {
    var quirks = {};
    var names = [];

    this.forEachUnlockedSkill(function(section, col, row) {
        var cell = this.getSkillFromCell(section, col, row);
        if (!(cell.type in quirks)) {
            quirks[cell.type] = 0;
            names.push(cell.type);
        }
        // TODO: if cell.value is not a number...?
        quirks[cell.type] += this.getSkillValue(cell.value);
    });

    names.sort();

    var output = document.getElementById("QuirkRollup");
    this.clearElement(output);
    for (var n of names) {
        var row = document.createElement('tr');

        var col1 = document.createElement('td');
        col1.appendChild(document.createTextNode(n));
        row.appendChild(col1);

        var col2 = document.createElement('td');
        col2.appendChild(document.createTextNode(this.formatSkillValue(n, quirks[n])));
        col2.style.textAlign = 'right';
        row.appendChild(col2);

        output.appendChild(row);
    }
};

MwoSkills.prototype.getStyleForCssId = function(selector, style) {
    for (var i = 0, l = document.styleSheets.length; i < l; i++) {
        var rules = null;
        try {
            var sheet = document.styleSheets[i];
            if (sheet.cssRules) {
                rules = sheet.cssRules;
            }
            else if (sheet.rules) {
                rules = sheet.rules;
            }
        }
        catch (e) {
            // cross-domain access failure?
            rules = null;
        }

        if (rules) {
            for (var j = 0, k = rules.length; j < k; j++) {
                var rule = rules[j];
                if (rule.selectorText && rule.selectorText.split(',').indexOf(selector) !== -1) {
                    return rule.style[style];
                }
            }
        }
    }
    return null;
};

MwoSkills.prototype.makeModal = function(dialogid, buttonid, callback) {
    var bound = function(){};
    if (callback) {
        bound = callback.bind(this);
    }

    var me = this;
    var modal = document.getElementById(dialogid);

    var donecb = function() {
        modal.style.display = "none";
        window.onclick = undefined;
        me.activeModalCb = undefined;
        bound();
    };

    var closer = modal.getElementsByClassName("close")[0];
    closer.onclick = donecb;

    var doit = function() {
        modal.style.display = "block";
        window.onclick = function(event) {
            if (event.target == modal) {
                donecb();
             }
        }
        me.activeModalCb = donecb;
    }

    if (buttonid) {
        var button = document.getElementById(buttonid);
        button.style.cursor = "pointer";
        button.onclick = doit;
    }
    else {
        doit();
    }

    return donecb;
};

MwoSkills.prototype.hookupCall = function(id, evt, cb) {
    if (typeof(id) == "string") {
        id = document.getElementById(id);
    }
    if (id) {
        id.addEventListener(evt, cb.bind(this));
    }
};

MwoSkills.prototype.generateContent = function(content, info) {
    this.clearElement(content);
    for (var i of info) {
        if (typeof(i) == "string") {
            i = document.createTextNode(i);
        }
        content.appendChild(i);
    }
};

MwoSkills.prototype.changedState = function() {
    if (location.search.indexOf('?') == 0) {
        window.history.pushState(null, "modified", location.href.split('?')[0]);
        this.lastPermId = null;
    }

    this.saveSessionState();

    this.updateSkillCount();
    this.computeQuirkRollup();
};

MwoSkills.prototype.saveSessionState = function() {
    sessionStorage.setItem('section', this.activeSection);
    sessionStorage.setItem('selections', JSON.stringify(this.unlockedSkills));
    sessionStorage.setItem('faction', this.selectedFaction);
    sessionStorage.setItem('weight', this.selectedWeight);
};

MwoSkills.prototype.loadSessionState = function(preset, section, mwohash) {
    if (section) {
        this.activeSection = section;
    }
    else if ('section' in sessionStorage) {
        this.activeSection = sessionStorage.section;
    }

    if ('faction' in sessionStorage) {
        this.selectedFaction = sessionStorage.faction;
        var factionSelect = document.getElementById("factionSelect");
        factionSelect.value = this.selectedFaction;
    }
    if ('weight' in sessionStorage) {
        this.selectedWeight = sessionStorage.weight;
        var weightSelect = document.getElementById("weightSelect");
        weightSelect.value = this.selectedWeight;
    }

    if (mwohash) {
        mwohash.trim();

        this.lastPermId = mwohash;

        var me = this;
        var donecb = function() {
            var imp = new MwoSkillImporter();
            var nodes = imp.deserialize(mwohash);

            me.unlockedSkills = [];
            for (var n of nodes) {
                var name = n[1].toLowerCase();
                for (var c of me.skillData[n[0]].cells) {
                    if (c.type.toLowerCase() == name && c.level == n[2]) {
                        var key = n[0] + "," + c.col + "," + c.row;
                        me.unlockedSkills.push(key);
                    }
                }
            }

            this.updateSkillCount();
            this.computeQuirkRollup();

            this.setActiveSection(this.activeSection);
        };

        if (Object.keys(this.skillData).length > 0) {
            donecb();
        }
        else {
            this.pendingSkillLoad = donecb;
        }

    }
    else if (preset) {
        var mainpage = document.getElementById("Display");
        mainpage.style.display = "none";

        var content = document.getElementById("generalContent");
        this.generateContent(content, ["Loading..."]);

        var donecb = this.makeModal("GeneralModal", null, function() {
            mainpage.style.display = "block";
        });

        this.loadJSON(preset, function(data, site) {
            if (data) {
                if (Object.keys(this.skillData).length > 0) {
                    this.unlockedSkills = this.deserializeUnlocked(data);

                    this.updateSkillCount();
                    this.computeQuirkRollup();

                    this.setActiveSection(this.activeSection);
                }
                else {
                    var me = this;
                    this.pendingSkillLoad = function() {
                        me.unlockedSkills = me.deserializeUnlocked(data);
                    };
                }
                donecb();
            }
            else {
                this.generateContent(content, [
                        "Error Loading \u2639",
                        document.createElement('br'),
                        "Could not fetch data from " + site + ".",
                        document.createElement('br'),
                        "It's likely the JSON is gone, sorry.",
                ]);
            }
        });
    }
    else if ('selections' in sessionStorage) {
        this.unlockedSkills = JSON.parse(sessionStorage.selections);
    }
};

MwoSkills.prototype.serializeUnlocked = function() {
    var data = {};
    this.forEachUnlockedSkill(function(section, col, row) {
        if (!(section in data)) {
            data[section] = [];
        }

        var skill = this.getSkillFromCell(section, col, row);
        data[section].push([skill.type, skill.level]);
    });
    return data;
};

MwoSkills.prototype.deserializeUnlocked = function(data) {
    var unlocked = this.deserializeUnlockedV1(data);
    if (!unlocked) {
        unlocked = this.deserializeNbarnes(data);
    }

    return unlocked ? unlocked : [];
};

MwoSkills.prototype.deserializeUnlockedV1 = function(data) {
    try {
        if (data.mwoskill != 'mwoskill') {
            return null;
        }

        // over time, some skills change name. map them
        function equivalentSkill(base, unlocked) {
            if (base == unlocked) {
                return true;
            }

            var equivs = [
                ['Enhanced UAC/RAC', 'UAC Jam Chance'],
            ];
            for (var e of equivs) {
                if (e.indexOf(base) >= 0 && e.indexOf(unlocked) >= 0) {
                    return true;
                }
            }

            return false;
        }

        data = data.selected;

        var unlocked = [];
        for (var section in data) {
            if (!(section in this.skillData)) {
                continue;
            }

            // REVIEW: not really happy about this loop
            for (var skill of data[section]) {
                for (var c of this.skillData[section].cells) {
                    if (equivalentSkill(c.type, skill[0]) && c.level == skill[1]) {
                        var key = section + "," + c.col + "," + c.row;
                        unlocked.push(key);
                    }
                }
            }
        }
        return unlocked;
    }
    catch (e) {
        console.log(e);
        return null;
    }
};

MwoSkills.prototype.deserializeNbarnes = function(data) {
    function translateSection(name) {
        switch (name) {
            case 'Survival':
                return 'Armor Structure';
            case 'Mobility':
                return 'Agility';
            case 'Jump Jets':
                return 'Jump Capabilities';
            case 'Operations':
                return 'Mech Operations';
            case 'Sensors':
                return 'Sensor Systems';
            case 'Auxiliary':
                return 'Miscellaneous';
            default:
                return name;
        }
    }
    function translateSkill(name) {
        switch (name) {
            case 'adv. salvos':
                return 'advanced salvos';
            default:
                return name;
        }
    }

    try {
        var unlocked = [];
        for (var module of data) {
            var name = translateSection(module.name);
            if (!(name in this.skillData)) {
                continue;
            }

            for (var skill of module.nodes) {
                if (!skill.s) {
                    continue;
                }

                var pieces = skill.id.split('-');

                var value = 0;
                if (/^\d+$/.test(pieces[pieces.length-1])) {
                    value = Number.parseInt(pieces[pieces.length-1]);
                    pieces.pop();
                }
                var field = translateSkill(pieces.join(' ').toLowerCase());

                for (var c of this.skillData[name].cells) {
                    if (c.type.toLowerCase() == field && c.level == value) {
                        var key = name + "," + c.col + "," + c.row;
                        unlocked.push(key);
                    }
                }
            }
        }
        console.log("loaded nbarnes");
        return unlocked;
    }
    catch (e) {
        console.log(e);
        return null;
    }
};

MwoSkills.prototype.enableEditor = function() {
    this.editor = true;
    var me = this;

    this.oldSkills = {};

    var skillModal = document.querySelector("#SelectVersionModal div.modalContent");

    var textarea = document.createElement("textarea");
    textarea.id = "SelectVersionEditText";
    skillModal.append(textarea);

    var buttonload = document.createElement("input");
    buttonload.id = "SelectVersionEditLoad";
    buttonload.value = "Load";
    buttonload.type = "button";
    skillModal.append(buttonload);
    buttonload.onclick = function() {
        var obj = JSON.parse(textarea.value);
        me.skillData[me.activeSection] = obj;

        me.updateSkillCount();
        me.computeQuirkRollup();

        me.renderSection(me.activeSection);
    }

    var buttongen = document.createElement("input");
    buttongen.id = "SelectVersionEditGen";
    buttongen.value = "Generate";
    buttongen.type = "button";
    skillModal.append(buttongen);
    buttongen.onclick = function() {
        var json = JSON.stringify(me.skillData[me.activeSection]);
        textarea.value = json;
    }

    this.debug = true;
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.disableEditor = function() {
    this.editor = false;

    var textarea = document.getElementById("SelectVersionEditText");
    if (textarea) {
        textarea.parentElement.removeChild(textarea);
    }

    var buttonload = document.getElementById("SelectVersionEditLoad");
    if (buttonload) {
        buttonload.parentElement.removeChild(buttonload);
    }

    var buttongen = document.getElementById("SelectVersionEditGen");
    if (buttongen) {
        buttongen.parentElement.removeChild(buttongen);
    }

    this.debug = false;
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.editorAddTile = function(tile) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (!skill) {
        var key = this.activeSection + "," + tile.col + "," + tile.row;

        if (key in this.oldSkills) {
            skill = this.oldSkills[key];
            delete this.oldSkills[key];
        }
        else {
            skill = {
                row: tile.row,
                col: tile.col,
                type: "",
                level: 0,
                value: 0,
                links: [],
            };
        }

        this.skillData[this.activeSection].cells.push(skill);

        this.renderSection(this.activeSection);
    }
};

MwoSkills.prototype.editorDeleteTile = function(tile) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (skill) {
        var key = this.activeSection + "," + tile.col + "," + tile.row;
        this.oldSkills[key] = skill;

        var index = this.skillData[this.activeSection].cells.indexOf(skill);
        if (index >= 0) {
            this.skillData[this.activeSection].cells.splice(index, 1);
        }

        this.renderSection(this.activeSection);
    }
};

MwoSkills.prototype.editorEditTile = function(tile) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (skill) {
        var field = window.prompt("Skill?", skill.type);
        var level = window.prompt("Level?", skill.level);
        var value = window.prompt("Value?", skill.value);

        skill.type = field.trim();
        skill.level = parseInt(level);
        skill.value = parseFloat(value);

        this.renderSection(this.activeSection);
    }
};

MwoSkills.prototype.editorAddLink = function(tile, direction) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (skill) {
        var linkrow = tile.row;
        if (direction == 0) {
            linkrow += 1;
        }
        else {
            linkrow += (tile.col % 2 == 0) ? 1 : 0;
        }

        var linkcol = tile.col + direction;

        var index = 0;
        while (index < skill.links.length) {
            var l = skill.links[index];
            if (l.col == linkcol && l.row == linkrow) {
                break;
            }
            index++;
        }

        if (index == skill.links.length) {
            skill.links.push({col: linkcol, row: linkrow});
        }
        else {
            skill.links.splice(index, 1);
        }

        this.renderSection(this.activeSection);
    }
};
